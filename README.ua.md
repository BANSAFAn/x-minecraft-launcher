<p align="center">
  <a href="https://xmcl.app" target="_blank">
    <img alt="Logo" width="100" src="xmcl-electron-app/icons/dark@256x256.png">
  </a>
</p>

<p align="center">
  <a href="https://github.com/Voxelum/x-minecraft-launcher">
    <img src="https://github.com/Voxelum/x-minecraft-launcher/workflows/Build/badge.svg" alt="Build">
  </a>
  <a href="https://github.com/Voxelum/x-minecraft-launcher/blob/master/LICENSE">
    <img src="https://img.shields.io/npm/l/@xmcl/core.svg" alt="License">
  </a>
  <a href="https://conventionalcommits.org">
    <img src="https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg" alt="Commit">
  </a>
  <br>
  <a href="https://discord.gg/W5XVwYY7GQ">
    <img src="https://discord.com/api/guilds/405213567118213121/widget.png" alt="Discord">
  </a>
  <a href="https://kook.top/gqjSHh">
    <img src="https://img.shields.io/endpoint?url=https://api.xmcl.app/kook-badge" alt="Kook">
  </a>
  <a href="https://afdian.net/@ci010">
    <img src="https://img.shields.io/endpoint?url=https://api.xmcl.app/afdian-badge" alt="afdian">
  </a>
  <a href="https://patreon.com/xmcl">
    <img src="https://img.shields.io/endpoint.svg?url=https%3A%2F%2Fshieldsio-patreon.vercel.app%2Fapi%3Fusername%3Dxmcl%26type%3Dpledges" alt="patreon">
  </a>
</p>

![home](assets/home.png)

Відвідайте [офіційний сайт](https://xmcl.app) щоб завантажити програму!

Якщо у вас є winget, ви можете використовувати winget для встановлення

```bash
winget install CI010.XMinecraftLauncher
```

[中文 README](README.zh.md)
[Ukraine README](README.ua.md)

## Особливості

- 📥 **Завантажити та автозаповнити**. Підтримка завантаження `Minecraft`, `Forge`, `Fabric`, `Quilt`, `OptiFine`, `JVM` з офіційних або сторонніх дзеркал.
- ⚡️ **Швидке завантаження**. Повторне використання сокета через агенти HTTP/HTTPS і завантаження файлів частинами одночасно.
- 💻 **Крос-платформа**. Панель запуску заснована на Electron і підтримує 🗔 Windows 10/11, 🍎 MacOS і 🐧 Linux.
- 📚 **Кілька екземплярів**. Користувачі можуть створювати кілька екземплярів, щоб ізолювати різні версії, модифікації та налаштування запуску.
- 🗂 **Керуйте всіма ресурсами**. Використовуйте (жорсткі/символічні) посилання для інсталяції ресурсів в екземпляри, підтримуючи оптимальне використання диска. Скрізь немає копій модів! 😆
- 🔥 **Вбудована підтримка CurseForge, Modrinth**. Ви можете завантажити ресурси всередині панелі запуску.
- 📦 **Підтримка імпорту/експорту** пакетів модів CurseForge & Modrinth із відповідністю!
- 🔒 **Підтримка кількох систем облікових записів**. Вбудований логін Microsoft і Mojang Yggdrasil API. Він також має вбудовану підтримку [ely.by](https://ely.by/) і [littleskin.cn](https://littleskin.cn). Ви також можете додати сторонні сервери автентифікації!
- 🔗 **Пірінгове з’єднання між користувачами**. Ви можете грати в багатокористувацьку мережу через локальну мережу, навіть не перебуваючи в одній фізичній мережі!
- 🔑 **Кодовий знак і сучасна упаковка**. У Windows ви можете використовувати `appx` і `appinstaller` для встановлення програми. Ви більше не отримуватимете повідомлення про блокування від свого браузера чи бачитимете помилки SmartScreen! 😎

## Внести свій внесок

Для загального розробника див. [Внесок](./CONTRIBUTING.md)

Для розробників локалізації i18n дотримуйтесь [Початок роботи з локалізацією](./CONTRIBUTING.i18n.md)

## ЛІЦЕНЗІЯ

[МОЯ](LICENSE)

## Спонсор (AFDIAN)

<!-- afdian-start -->
<div style="display: flex; align-items: center; justify-items:center; gap: 0.2em; flex-wrap: wrap;">
<a title="Vulcankta: ￥390.00" href="https://afdian.net/u/9d663ec6fb6711ec9ace52540025c377"> <img width="100" height="100" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/9d663ec6fb6711ec9ace52540025c377/avatar/22b173dd893745d7a9e8431a0d91b3e3_w7680_h8128_s5344.png"> </a>
<a title="ahdg: ￥180.00" href="https://afdian.net/u/dd9058ce20df11eba5c052540025c377"> <img width="70" height="70" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/dd9058ce20df11eba5c052540025c377/avatar/0c776e6de1b1027e951c6d94919eb781_w1280_h1024_s364.jpg"> </a>
<a title="Kandk: ￥30.00" href="https://afdian.net/u/404b86a078e111ecab3652540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/404b86a078e111ecab3652540025c377/avatar/dfa3e35a696d8d8af5425dd400d68a8d_w607_h527_s432.png"> </a>
<a title="白雨 楠: ￥30.00" href="https://afdian.net/u/7f6ad7161b3e11eb8d0e52540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/7f6ad7161b3e11eb8d0e52540025c377/avatar/1fa3b75648a15aea8da202c6108d659b_w1153_h1153_s319.jpeg"> </a>
<a title="圣剑: ￥30.00" href="https://afdian.net/u/ef50bc78b3d911ecb85352540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/user_upload_osl/8a1c4eb2e580b4b8b463ceb2114b6381_w132_h132_s3.jpeg"> </a>
<a title="同谋者: ￥30.00" href="https://afdian.net/u/7c3c65dc004a11eb9a6052540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/default/avatar/avatar-blue.png"> </a>
<a title="染川瞳: ￥5.00" href="https://afdian.net/u/89b1218c86e011eaa4d152540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/89b1218c86e011eaa4d152540025c377/avatar/9bf08f81d231f3054c98f9e5c1c8ce40_w640_h640_s57.jpg"> </a>
<a title="爱发电用户_CvQb: ￥5.00" href="https://afdian.net/u/177bea3cf47211ec990352540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/default/avatar/avatar-purple.png"> </a>
<a title="水合: ￥5.00" href="https://afdian.net/u/039508f2b17d11ebad1052540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/default/avatar/avatar-orange.png"> </a>
<a title="Jisoadng: ￥5.00" href="https://afdian.net/u/0c5c865e08ee11ecba1352540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/user/0c5c865e08ee11ecba1352540025c377/avatar/b7ae9f15fc461e68c4b9a853ee966a27_w448_h448_s290.png"> </a>
<a title="DIO: ￥5.00" href="https://afdian.net/u/7ac297b4722211eab4a752540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/default/avatar/avatar-purple.png"> </a>
<a title="爱发电用户_DJpu: ￥5.00" href="https://afdian.net/u/8c23a236cf7311ec9c3452540025c377"> <img width="50" height="50" style="border-radius: 100%" src="https://pic1.afdiancdn.com/default/avatar/avatar-purple.png"> </a>
</div>
<!-- afdian-end -->

## Кредит

[GodLeaveMe](https://github.com/GodLeaveMe), ведення реєстру пакетів AUR.

[0xc0000142](https://github.com/0xc0000142), обслуговування крила.

[vanja-san](https://github.com/vanja-san), який надає російську мову!

[lukechu10](https://github.com/lukechu10) і [HoldYourWaffle](https://github.com/HoldYourWaffle) допомагають мені в ядрі програми запуску.

[laolarou726](https://github.com/laolarou726), який дуже допомагає в розробці панелі запуску.

Також окрема подяка

[Yricky](https://github.com/Yricky), [Jin](https://github.com/Indexyz), [LG](https://github.com/LasmGratel), [Phoebe](https ://github.com/PhoebezZ), [Sumeng Wang](https://github.com/darkkingwsm), [Luca](https://github.com/LucaIsGenius), [Charles Tang](https:// github.com/CharlesQT)
